export default {
    async exec(action, isWorking) {
        try {
            isWorking(true);

            return await action();
        } finally {
            isWorking(false);
        }
    }
};
