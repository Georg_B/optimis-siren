import _ from 'lodash';

const { isFunction } = _;

export default {
	toLazy(obj) {
        let result = {};

        for (let key in obj) {
            let prop = obj[key];

            if (isFunction(prop)) {
                Object.defineProperty(result, key, {
                    get() {
                        return prop();
                    }
                });
            } else {
                result[key] = prop;
            }
        }

        return result;
    }
};
