import Ember from 'ember';
import layout from '../templates/components/siren-checkbox';
import AttributeWatchMixin from '../mixins/attribute-watch-mixin';
import _ from 'lodash';
import RSVP from 'rsvp';

const { Component, run, isPresent } = Ember;
const { isFunction, isString } = _;
const { Promise } = RSVP;

export default Component.extend(AttributeWatchMixin, {
    layout,
    tagName: '',

    _attrs: {
        id: { validate: v => isString(v) && isPresent(v), default: function() { return this.uuid.generate(); } },
        value: { normalize: v => v === 'false' ? false : !!v },
        disabled: { normalize: v => v === 'false' ? false : !!v },
        readonly: { use$: v => !!v, isProp: true },
        onClick: { validate: isFunction, default: () => () => { } },
        onChange: { validate: isFunction, default: () => () => { } }
    },

    actions: {
        clicked(e) {
            let readonly = this.get('readonly');
            
            if (readonly) {
                return false;
            }

            let onClick = this.get('onClick');
            let onChange = this.get('onChange');
            let onClickResult = onClick();

            return Promise.resolve(onClickResult).then(() => {
                run(null, onChange, e.target.checked);
            }).catch(error => {
                e.target.checked = !e.target.checked;

                return Promise.reject(error);
            });
        }
    }
});
