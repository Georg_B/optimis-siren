import Ember from 'ember';
import moment from 'moment';
import _ from 'lodash';
import AttributeWatchMixin from 'optimis-siren/mixins/attribute-watch-mixin';
import layout from '../templates/components/siren-table';
import { validateBoolean, validateFunction, validateArray, validatePlainObject } from 'optimis-siren/mixins/attribute-watch-mixin';

const { Component, $, run, computed, get, set, isPresent, merge } = Ember;
const { isString, sortBy } = _;

export default Component.extend(AttributeWatchMixin, {
    layout,
	tagName: 'table',

    classNameBindings: ['orderable'],

    initAttrs() {
        return merge(this._super(...arguments), {
            columns: validateArray(),
            data: validateArray(),
            emptyDataMessage: validateBoolean(true),
            orderable: validateBoolean(),           
            draggable: validateBoolean(),
            isLoadingData: validateBoolean(),
            onDragFn: validateFunction(),
            onDropFn: validateFunction(),
            onOverFn: validateFunction(),            
            orderBy: validatePlainObject(),
            orderFn: validateFunction((data) => {
                let name = this.get('orderBy.name');
                let order = this.get('orderBy.order');

                if (name) {
                    return sortBy(data, [name], [order]);
                }

                return data;
            })            
        });
    },

    hasData: computed('data.[]', function() {
        let data = this.get('data');

        return isPresent(data);
    }),

    orderedData: computed('data.[]', 'orderBy.{name,order}', 'updatedAt', function() {
        let data = this.get('data');
        let orderFn = this.get('orderFn');

        return orderFn(data);
    }),

    columnsData: computed('columns.[]', 'orderBy.{name,order}', function() {
        let columns = this.get('columns');
        let orderBy = this.get('orderBy') || {};

        return columns.map(name => {
            let c = '';
            let o = name;
            let orderable = true;

            if (!isString(name)) {
                o = name.orderBy || name.name;
                orderable = name.orderable;
                c = name.class || '';
                name = name.name;
            }           

            if (get(orderBy, 'name') === o) {
                c += get(orderBy, 'order') === 'asc'
                    ? ' orderable_ordered'
                    : ' orderable_ordered_reverse';
            }

            if (orderable === false) {
                c += ' not_orderable';
            }

            return { name, orderBy: o, class: c, orderable };
        });
    }),

	didRender() {
		this._super(...arguments);

		let $element = this.$();
		let draggable = this.get('draggable');
		let onDragFn = this.get('onDragFn');
		let onDropFn = this.get('onDropFn');
		let onOverFn = this.get('onOverFn');

		if (draggable && !onOverFn) {
			onOverFn = (e, r) => run(this, this._onOverFn, e, r);
		}

		if (draggable) {
			$element.find('> tbody > tr').each((i, tr) => {
				let $tr = $(tr);

				$tr.draggable({
					containment: 'parent',
					start: onDragFn,
					refreshPositions: true,
					scrollSensitivity: 5
				});

				$tr.droppable({
					drop: onDropFn,
					over: onOverFn,
					hoverClass: 'ui-drop-hover'
				});
			});
		}
	},

	_onOverFn(e, row) {
		let data = this.get('data');

		let $dragRow = $(row.helper[0]);
		let $overRow = $(e.target);
		let dragId = $dragRow.attr('id');
		let overId = $overRow.attr('id');
		let drag = data.findBy(orderOptions.findId, dragId);
		let over = data.findBy(orderOptions.findId, overId);

		let dragOrder = get(drag, orderOptions.orderPoperty);
		let overOrder = get(over, orderOptions.orderPoperty);

		set(drag, orderOptions.orderPoperty, overOrder);
		set(over, orderOptions.orderPoperty, dragOrder);

        this.set('updatedAt', moment());
	},

    actions: {
        updateOrderBy(column) {
            let orderBy = this.get('orderBy');

            if (orderBy.name === column) {

                switch (get(orderBy, 'order')) {
                    case 'asc':
                        set(orderBy, 'order', 'desc');
                    break;
                    
                    case 'desc':
                        set(orderBy, 'name', undefined);
                        set(orderBy, 'order', undefined);
                    break;
                }
            } else {
                set(orderBy, 'name', column);
                set(orderBy, 'order', 'asc');
            }
        }
    }
});
