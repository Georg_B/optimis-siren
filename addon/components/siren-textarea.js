import Ember from 'ember';
import layout from '../templates/components/siren-textarea';
import AttributeWatchMixin from '../mixins/attribute-watch-mixin';
import _ from 'lodash';

const { Component, $, isPresent, run } = Ember;
const { isNumber, isString } = _;
const { once } = run;

export default Component.extend(AttributeWatchMixin, {
    layout,
    tagName: '',

    init() {
        this._super(...arguments);
        this.set('id', this.uuid.generate());
    },

    _attrs: {
        id: { default: function() { return this.get('id'); } },
        placeholder: { use$: isPresent },
        cols: { use$: isNumber },
        rows: { use$: isNumber },
        wrap: { use$: isPresent },
        maxLength: { use$: isNumber },
        disabled: { use$: v => !!v, isProp: true },
        readonly: { use$: v => !!v, isProp: true },
        onChange: {},
        value: {},
        class: { use$: v => isString(v) && isPresent(v) },
        name: { use$: v => isString(v) && isPresent(v) }
    },

    didInsertElement() {
        let id = this.get('id');
        let $textarea = $(`#${id}`);
        let onChange = this.get('onChange');

        $textarea.bind('propertychange change click keyup input paste', () => {
            let value = $textarea.val();

            once(this, onChange, value);
        });
    },

    getElement() {
        let id = this.get('id');
        let $textarea = $(`#${id}`);

        return $textarea;
    }
});
