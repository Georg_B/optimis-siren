import Ember from 'ember';
import AttributeWatchMixin from 'optimis-siren/mixins/attribute-watch-mixin';
import layout from '../templates/components/siren-autocomplete';
import _ from 'lodash';
import { validateString, validateBoolean, validateFunction } from 'optimis-siren/mixins/attribute-watch-mixin';

const { $, isArray, run, computed, merge, guidFor } = Ember;
const { debounce } = run;
const { isFunction } = _;

export default Ember.Component.extend(AttributeWatchMixin, {
    layout,
    tagName: '',

    initAttrs() {
        return merge(this._super(...arguments), {
            id: { default: () => guidFor() },
            label : validateString(),
            value: {},
            placeholder: validateString(),
            displayFn: validateFunction(x => (x || '') + ''),
            disabled: validateBoolean(),
            readonly: validateBoolean(),
            source: { validate: isArray },
            filterFn: validateFunction(x => x),
            sourceFn: { validate: isFunction },
            onChangeFn: validateFunction(),
            onSelectFn: validateFunction()            
        });
    },

    didInsertElement() {
        this._super(...arguments);
        this.set('isSelected', false);

        let id = this.get('id');
        let $div = $(`#${id}-autocomplete-div`);
        let $auto = $(`#${id}-autocomplete`);

        $div.focusout(() => {
            $auto.hide(800);
        });
    },

    stringValue: computed('value', function() {
        let displayFn = this.get('displayFn');
        let value = this.get('value');

        return displayFn(value);
    }),

    didReceiveAttrs() {
        this._super(...arguments);

        let source = this.get('source') || this.get('filteredList') || [];

        this.set('filteredList', source);
    },

    async updateSourceAsync(query) {
        let sourceFn = this.get('sourceFn');
        let result = await sourceFn(query);

        this.set('filteredList', result);
    },

    actions: {
        select(item) {
            let onSelectFn = this.get('onSelectFn');
            let id = this.get('id');
            let $auto = $(`#${id}-autocomplete`);

            $auto.css('display', 'none');
            this.set('lastQuery', item);
            this.set('selected', true);

            onSelectFn(item);
        },

        filter(query) {
            let lastQuery = this.get('lastQuery');
            let selected = this.get('selected') | false;
            let onChangeFn = this.get('onChangeFn');

            onChangeFn(query);

            if (selected && lastQuery === query) {
                return;
            }

            this.set('selected', false);
            this.set('lastQuery', query);
            this.set('value', query);

            let source = this.get('source');
            let filterFn = this.get('filterFn');
            let sourceFn = this.get('sourceFn');
            let id = this.get('id');
            let $auto = $(`#${id}-autocomplete`);

            $auto.css('display', 'block');

            if (lastQuery === query) {
                return;
            }

            if (isFunction(sourceFn)) {
                debounce(this, this.updateSourceAsync, query, 500);
            } else if (isArray(source)) {
                let filteredList = source.filter(filterFn);

                this.set('filteredList', filteredList);
            }
        }
    }
});
