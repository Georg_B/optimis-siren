import Ember from 'ember';
import layout from '../templates/components/siren-base64-image';
import AttributeWatchMixin from '../mixins/attribute-watch-mixin';
import _ from 'lodash';

const { Component, computed } = Ember;
const { isString, isInteger } = _;

export default Component.extend(AttributeWatchMixin, {
    layout,
    tagName: '',

    _attrs: {
        alt: {},
        width: { validate: isInteger, default: 100 },
        height: { validate: isInteger, default: 100 },
        mime: { validate: isString, default: 'image/jpeg' },
        charset: { validate: isString, default: 'utf-8' },
        data: {}
    },

    src: computed('mime', 'charset', 'data', function() {
        return 'data:' + this.get('mime') + ';charset:' + this.get('charset') + ';base64,' + this.get('data');
    }),

    didReceiveAttrs() {
        this._super(...arguments);

        let data = this.get('data');

        if (isString(data)) {
            let image = new Image();

            image.src = this.get('src');

            this.set('width', image.width);
            this.set('height', image.height);
        }
    }
});
