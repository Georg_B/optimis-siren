import Ember from 'ember';

const EmberError = Ember.Error;

export function FetchError(message) {
	EmberError.call(this, message);
}

FetchError.prototype = Object.create(EmberError.prototype);

export function CameraNotAvailableError(message) {
	EmberError.call(this, message);
}

CameraNotAvailableError.prototype = Object.create(EmberError.prototype);

export default { EmberError, CameraNotAvailableError };
