import Ember from 'ember';
import BaseAuthenticator from 'ember-simple-auth/authenticators/base';
import _ from 'lodash';
import RSVP from 'rsvp';
import authToken from 'optimis-siren/utils/auth-token';

const { run, get, isPresent } = Ember;
const { isString, isFunction } = _;
const { Promise } = RSVP;

export default BaseAuthenticator.extend({
    loginUrl: 'authentication/login',
    getPublicKeyUrl: 'authentication/publicKey',
    mustUseSecurePassword: true,
    getSessionDataFromToken: null,

    restore(data) {
        return new Promise((resolve, reject) => {
            let token = get(data, 'token');
            let hasToken = isString(token) && isPresent(token);

            if (hasToken) {
                run(null, resolve, data);
            } else {
                run(null, reject);
            }
        });
    },

    invalidate() {
        return new Promise((resolve) => {
            run(null, resolve);
        });
    },

    authenticate(email, password) {
        return new Promise((resolve, reject) => {
            let mustUseSecurePassword = this.get('mustUseSecurePassword') !== false;
            let getPublicKey = mustUseSecurePassword ? this.getPublicKey() : Promise.resolve();

            getPublicKey.then(publicKey => {
                return this.login(email, password, publicKey);
            }).then(token => {
                let sessionData = isFunction(this.getSessionDataFromToken) ? this.getSessionDataFromToken(token) : authToken.getSessionDataFromToken(token);

                run(null, resolve, sessionData);
            }).catch(error => {
                run(null, reject, error);
            });
        });
    },

    async getPublicKey() {
        let [publicKey] = await this.fetch.get(this.get('getPublicKeyUrl'));

        return publicKey;
    },

    async login(email, password, publicKey) {
        let isSecure = isString(publicKey) && publicKey.length > 0;
        let payload = {
            email,
            password: isSecure ? this.crypto.rsaEncrypt(password, publicKey) : password,
            isSecure,
            mustUseCookie: true
        };
        let [token] = await this.fetch.post(this.get('loginUrl'), undefined, payload);

        return token;
    }
});
