/* globals forge */
import Ember from 'ember';

const { Service } = Ember;

export default Service.extend({
    rsaEncrypt(plainText, pemPublicKey) {
        let publicKey = forge.pki.publicKeyFromPem(pemPublicKey);
        let utf8PlainText = forge.util.encodeUtf8(plainText);
        let byteString = this._getForgeByteString(utf8PlainText);
        let data = publicKey.encrypt(byteString, 'RAW');
        let base64Data = forge.util.encode64(data);

        return base64Data;
    },

    _getForgeByteString(str) {
        return (new forge.util.ByteBuffer(str)).getBytes();
    }
});
