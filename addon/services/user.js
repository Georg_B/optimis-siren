import Ember from 'ember';

const { Service, computed } = Ember;
const { readOnly } = computed;

export default Service.extend({
    isAuthenticated: readOnly('session.isAuthenticated'),
    userAccountId: readOnly('session.data.authenticated.userAccountId'),
    token: readOnly('session.data.authenticated.token')
});
