import Ember from 'ember';
import { uuid } from 'optimis-siren/utils/uuid';

const { Service } = Ember;

export default Service.extend({
    generate() {
        return uuid.generate();
    },

    fromNumber(value) {
        return uuid.fromNumber(value);
    }
});
