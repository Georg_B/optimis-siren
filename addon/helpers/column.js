import Ember from 'ember';
import _ from 'lodash';

const { Helper } = Ember;
const { isUndefined, isNull } = _;

export default Helper.extend({
	compute(ignore, obj) {
        let result = { 
            name: obj.name, 
            orderBy: obj.orderBy, 
            orderable: obj.orderable, 
            class: obj.class
        };

        let orderable = result.orderable;
        if (isUndefined(orderable) || isNull(orderable)) {
            orderable = !!orderable;
        }

        result.orderable = orderable;

        return result;
	}
});
