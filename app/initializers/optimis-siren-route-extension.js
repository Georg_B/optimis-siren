import Ember from 'ember';
import AuthenticationRouteMixin from 'optimis-siren/mixins/authentication-route-mixin';
import AuthorizationRouteMixin from 'optimis-siren/mixins/authorization-route-mixin';
import ENV from '../config/environment';
import _ from 'lodash';

const { Route } = Ember;

export function initialize() {
    let isAuthenticationEnabled = _.get(ENV, 'authentication.isEnabled') !== false;

    if (isAuthenticationEnabled) {
        let isAuthorizationEnabled = _.get(ENV, 'authorization.isEnabled') !== false;

        if (isAuthorizationEnabled) {
            Route.reopen(AuthorizationRouteMixin);
        }

        Route.reopen(AuthenticationRouteMixin);
    }
}

export default {
    name: 'optimis-siren-route-extension',
    after: ['ember-data', 'ember-simple-auth', 'optimis-siren'],
    initialize
};
