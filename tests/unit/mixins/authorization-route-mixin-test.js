import Ember from 'ember';
import AuthorizationRouteMixinMixin from 'optimis-siren/mixins/authorization-route-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | authorization route mixin');

// Replace this with your real tests.
test('it works', function(assert) {
  let AuthorizationRouteMixinObject = Ember.Object.extend(AuthorizationRouteMixinMixin);
  let subject = AuthorizationRouteMixinObject.create();
  assert.ok(subject);
});
